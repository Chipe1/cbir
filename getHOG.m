%% load images
im_name = rdir('./Images/**/*.*');
im = cell(1,length(im_name));

for i = 1:length(im_name)
    i
    im{i} = imresize(rgb2gray(imread(im_name(i).name)), [256 256]);
end

%% get features

hog = [];

for i = 1:length(im_name)
    i
    feat = extractHOGFeatures(im{i});
    hog = [hog; feat];
end

save('./hog.mat', 'hog');

%% ALERT

amp=10;
fs=20500;  % sampling frequency
duration=10;
freq=10000;
values=0:1/fs:duration;
a=amp*sin(2*pi*freq*values);
sound(a);
