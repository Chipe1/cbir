function y = hist_dist(A, B)
    y = 0;
    for i = 1:length(A)
         m = min(A(i), B(i)) + 1;
         if m > 0
              y = y + ((A(i) - B(i))/m)^2;
         end
    end
end

