% run('~/Desktop/Matlab/extensions/vlfeat-0.9.20/toolbox/vl_setup');

% siftf = cell(1000, 1);
% slist = [];

khist = zeros(1000, 500);

for i = 1:1000
    i
%     im = single(rgb2gray(imread(strcat('images/',num2str(i-1),'.jpg'))));
%     [f, d] = vl_sift(im);
%     siftf{i} = d';
%     slist = [slist; siftf{i}];
    siftl = siftf{i};
    for j=1:length(siftl)
        [val, ind] = min(vl_alldist(single(siftl(j,:))', centers));
        khist(i,ind) = khist(i,ind) + 1;
    end
    khist(i,:) = khist(i,:)/length(siftl);
end

% [centers, assignments] = vl_kmeans(slist', k);
