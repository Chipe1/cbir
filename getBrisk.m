%% load images
im_name = rdir('./Images/**/*.*');
im = cell(1,length(im_name));

for i = 1:length(im_name)
    i
    im{i} = imresize(rgb2gray(imread(im_name(i).name)), [256 256]);
end

%% get features

sz = [];
brisk = [];

for i = 1:length(im_name)
    i
    pts = detectBRISKFeatures(im{i});
    [feats, xpts] = extractFeatures(im{i}, pts);
    sz = [sz feats.NumFeatures];
    brisk = [brisk; feats.Features];
end

save('./surf.mat', 'brisk', 'sz');

%% kmeans

for k = (1:3)*500
    k
    [assignment, centers] = kmeans(single(brisk), k);
    save(strcat('./kmean',int2str(k),'.mat'), 'assignment', 'centers');
end

%% ALERT

amp=10;
fs=20500;  % sampling frequency
duration=10;
freq=10000;
values=0:1/fs:duration;
a=amp*sin(2*pi*freq*values);
sound(a);

%% result
% 1 211
% 3 142
% 5 131