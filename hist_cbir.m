im = imread('images/943.jpg');
load('histinfo');
load('imname');

sz = size(im);
k=5;
closest = zeros(1, k);
hist = [imhist(im(:,:,1))' imhist(im(:,:,2))' imhist(im(:,:,3))']/(sz(1) * sz(2));

present = 1:length(histnfo);
for i=1:k
    corr = zeros(1,length(histnfo));
    for j = 1:length(histnfo)
                corr(j) = pdist2(histnfo(j,:), hist, 'correlation');
    end

    [val, xind] = min(corr);
    histnfo = [histnfo(1:xind-1, :); histnfo(xind+1:length(histnfo), :)];
    ind = present(xind);
    present = [present(1:xind-1) present(xind+1:length(present))];
    
    closest(i) = ind;
end

for i = 1:k
    subplot(1, 5, i);
    imshow(imname{closest(i)})
end