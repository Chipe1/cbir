run('~/Desktop/Matlab/extensions/vlfeat-0.9.20/toolbox/vl_setup');

im = imread('images/943.jpg');

load('imname');
load('siftinfo');

[f, d] = vl_sift(single(rgb2gray(im)));

h = zeros(1, 500);

for i = 1:size(d,2)
    [val, ind] = min(vl_alldist(single(d(:,i)), kcenters));
        h(ind) = h(ind) + 1;
end
h = h / sum(h);

k = 5;
closest = zeros(1, k);
histnfo = khist;
present = 1:length(histnfo);
for i=1:k
    corr = zeros(1,length(histnfo));
    for j = 1:length(histnfo)
                corr(j) = pdist2(histnfo(j,:), h);
    end

    [val, xind] = min(corr);
    histnfo = [histnfo(1:xind-1, :); histnfo(xind+1:length(histnfo), :)];
    ind = present(xind);
    present = [present(1:xind-1) present(xind+1:length(present))];
    
    closest(i) = ind;
end

for i = 1:k
    subplot(1, 5, i);
    imshow(imname{closest(i)})
end