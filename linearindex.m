function [actual,predicted] = linearindex(inp_mat, label, k)
    label = categorical(label);
    p = cvpartition(32*23, 'kfold', 23);
    actual = label;
    predicted = categorical(zeros(length(label), 1));
    for i=1:23
        i
        ind = p.test(i);
        test_ind = find(ind==1);
        train_ind = ind == 0;
        train_label = actual(train_ind);
        test = inp_mat(test_ind, :);
        train = inp_mat(train_ind, :);
        s_result = knnsearch(train, test, 'k', k);
        for j = 1:size(s_result, 1)
            predicted(test_ind(j)) = mode(train_label(s_result(j, :)));
        end
    end  
end